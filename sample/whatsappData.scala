import org.apache.spark.sql.functions.current_date
import org.apache.spark.sql.{DataFrame, SparkSession}

object whatsappData {

  def data_new(df:DataFrame): DataFrame ={
    val res_df = df.withColumn("date",current_timestamp())
    return res_df
  }
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("whatsapp").getOrCreate()
    val df = spark.read.format("csv").option("header","true").option("inferSchema","true").load("C:\\Users\\Sai Krishna\\Downloads\\archive (1)\\Whatsapp_chat.csv")
    val final_DF = data_new(df)
    final_DF.printSchema()
    final_DF.count()

    new processData(final_DF)
  }

}
